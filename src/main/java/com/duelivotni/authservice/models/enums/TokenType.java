package com.duelivotni.authservice.models.enums;

public enum TokenType {
    ACCESS,
    REFRESH
}
