package com.duelivotni.authservice.services;

public interface PasswordService {
    boolean matches(
            String encodedPassword ,
            String password);

    String encodePassword(String password);
}
