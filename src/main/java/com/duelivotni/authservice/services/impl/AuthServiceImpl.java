package com.duelivotni.authservice.services.impl;

import com.duelivotni.authservice.models.dtos.SecurityUser;
import com.duelivotni.authservice.models.enums.TokenType;
import com.duelivotni.authservice.models.responses.AuthResponse;
import com.duelivotni.authservice.services.AuthService;
import com.duelivotni.authservice.services.PasswordService;
import com.duelivotni.authservice.services.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl
        implements AuthService, Exceptional {

    private final UserService userService;
    private final PasswordService passwordService;

    private final TokenService tokenService;

    @Override
    public AuthResponse login(
            String email,
            String password) {

        var user = userService.findByEmail(email)
                .filter(e -> passwordService.matches(e.getPassword(), password))
                .orElseThrow(() -> bad(Messages.INVALID_CREDENTIALS));
        return authResponse(user);
    }

    @Override
    public SecurityUser currentUser() {
        return (SecurityUser) SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public User user() {
        return userService.getById(currentUser().getId());
    }

    @Override
    public AuthResponse refreshToken(String refreshToken) {
        String email = tokenService.getEmailFromRefreshToken(refreshToken);

        var type = tokenService.tokenType(refreshToken);

        if (!Objects.equals(type, TokenType.REFRESH)) {
            throw bad(TOKEN_INVALID);
        }

        var user = userService.findByEmail(email).orElseThrow(() -> bad(TOKEN_INVALID));

        return authResponse(user);
    }

    private AuthResponse authResponse(User user) {
        String token = tokenService.generateAccessToken(user.getId(), user.getEmail(), user.getRole().toString(), null);
        String refreshToken = tokenService.generateRefreshToken(user.getEmail());

        return new AuthResponse(user, token, refreshToken);
    }
}
