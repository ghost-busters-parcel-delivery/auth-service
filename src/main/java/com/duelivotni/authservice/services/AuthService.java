package com.duelivotni.authservice.services;

import com.duelivotni.authservice.models.dtos.SecurityUser;
import com.duelivotni.authservice.models.responses.AuthResponse;

public interface AuthService {
    AuthResponse login(
            String email ,
            String password);

    SecurityUser currentUser();

    User user();

    AuthResponse refreshToken(String refreshToken);
}
