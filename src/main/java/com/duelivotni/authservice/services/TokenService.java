package com.duelivotni.authservice.services;

import com.duelivotni.authservice.models.dtos.SecurityUser;
import com.duelivotni.authservice.models.enums.TokenType;
import io.jsonwebtoken.Claims;

import java.util.Optional;
import java.util.UUID;

public interface TokenService {
    String generateAccessToken(
            UUID id ,
            String email ,
            String role ,
            String location);

    String generateRefreshToken(String email);

    String getEmailFromRefreshToken(String refreshToken);

    String email(String token);

    Long id(String token);

    String role(String token);

    Optional<String> location(String token);

    String email(Claims claims);

    Long id(Claims claims);

    String role(Claims claims);

    Optional<String> location(Claims claims);

    SecurityUser securityUser(String token);

    TokenType tokenType(String token);

    TokenType tokenType(Claims claims);
}
